---
title: Professional Development Plan
subtitle: My plan for for a bright future 
bigimg: [{src: "/img/AOD_GIF.gif", desc: "Average Aerosol Optical Depth (AOD) of two-decade (2000-2019) over India at 1-km and 50-km spatial resolution "}]
comments: false
---

##What is the skills and knowledge I have or, my background?
I graduated with a bachelor’s degree in engineering from Uttar Pradesh Technical University in 2017.  I did some sensor based projects during my undergraduate and decided to peruse research as a career. After working some time with air pollution sensors and other sources of datasets I realised the gap in public policy and engineering solutions which often went overlooked especially in urban pollution mitigation field. This motivated me to enrol in Virginia Tech’s Master of Urban and Regional Planning (MURP) program.

##What is my current position?
Presently, I work with Dr. Steve Hankey in collecting urban pollution data and developing statistical & machine learning models for particle count number (PCN) and fine particulate matter (pm2.5).


