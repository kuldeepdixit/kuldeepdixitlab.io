---
title: Publications
subtitle: Published journal Article
#bigimg: [{src: "img/AOD_GIF.gif", desc: "Average Aerosol Optical Depth (AOD) of two-decade (2000-2019) over India at 1-km and 50-km spatial resolution "}]
comments: false
---

1- Meng Qi, Kuldeep Dixit, Julian D. Marshall, Wenwen Zhang, and Steve Hankey
Environmental Science & Technology 2022 56 (18), 13499-13509
DOI: 10.1021/acs.est.2c03581 ['HTML'](https://pubs.acs.org/doi/abs/10.1021/acs.est.2c03581)
