---
title: About me
subtitle: My background, history and all in between
bigimg: [{src: "img/AOD_GIF.gif", desc: "Average Aerosol Optical Depth (AOD) of two-decade (2000-2019) over India at 1-km and 50-km spatial resolution "}]
comments: false
---

I graduated with a Bachelor's degree in engineering from UPTU in 2017. Picked microcontroller programming as a hobby while pursuing engineering and made some functional IoT and Robotics project in college. Joined IIT-Kanpur as a research fellow and worked on low-cost sensors. Improved the sensor's performance and co-authored a few journal papers in credible journals. After some experience realized the demand for skills in data science and started working full time on spatial algorithm development and got an opportunity to work on a few more papers using machine learning and statistical modeling. Entered IIT-Delhi as a Data scientist and worked on different satellite-based geospatial datasets for estimating air quality. Took a lead role in developing a ground-level PM2.5 data repository with a 1-km resolution for India. Joined Virginia Tech as a graduate research assistant(GRA) and started working on urban air quality and the story continues ...

