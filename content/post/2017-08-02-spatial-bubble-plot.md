---
title: Geospatial bubble plotting
subtitle: plotting concentration of PM2.5 over Delhi using Cetral Pollution Control Board(CPCB) station data
date: 2017-05-26
bigimg: [{src: "img/DELHI_CPCB.gif", desc: "LHS-Landfill sites hight and size, RHS- Pollution over Delhi's regions"}]
---

Plotting quantities over a map is a critical component for analyzing the change of certain parameters over a geographical area. Python has many libraries for achieving this. This coding [`notebook`](https://github.com/Kuldeep252/HYSPLIT-Trajectories/blob/master/backtraj_plotting.ipynb) can guide you in getting started. You can read more about it [`here`](https://github.com/Kuldeep252/HYSPLIT-Trajectories).


