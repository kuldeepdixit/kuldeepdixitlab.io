---
title: HYSPLIT model mapping
subtitle: Plotting air parcel from model output
date: 2017-05-26
bigimg: [{src: "img/hs_traj.png", desc: "Tracing air parcel's path back in time for Delhi at three different levels in winter"}]
---

Mapping path and tracks is an important component of spatial analysis. The standard data is often in plain text format rater than in projected shape files.This coding [`notebook`](https://github.com/Kuldeep252/HYSPLIT-Trajectories/blob/master/backtraj_plotting.ipynb) can help you achieve that in no-time. You can read more about it [`here`](https://github.com/Kuldeep252/HYSPLIT-Trajectories).

Hope you find this helpful!
