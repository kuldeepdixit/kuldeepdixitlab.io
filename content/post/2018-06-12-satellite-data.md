---
title: Satellite data handling
subtitle: Extracting data from MODIS's MCD19A2 product
date: 2018-06-12
---

Satellite data is an emerging source of information for various scientific fields and its analysis is proving critical for policy formulation. The following coding [`notebook`](https://github.com/Kuldeep252/MAIAC_MODIS_data_handling_for_python/blob/master/Python_for_MODIS.ipynb) can help one in getting started with raster datasets. If one learns to use libraries like gdal/ogr, it is very easy to combine python with open GIS software like [QGIS](https://www.qgis.org/en/site/) for achieving more functionality. You can read more about it [`here`](https://github.com/Kuldeep252/MAIAC_MODIS_data_handling_for_python).

I will post more blogs on doing such analysis and what I have been able to achieve with such skills soon.


