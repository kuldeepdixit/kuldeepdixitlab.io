## Welcome to my Zone!
This website serves as a platform for publishing my blogs and capture my journey as a researcher and data scientist. To request professional work samples or for any inquiry please drop a mail on 'dixitkuldeep252{at}gmail{dot}com' and I will respond to you within **24 hours**. 

Check my publications on [`google scholar`](https://scholar.google.com/citations?user=g2-MMyYAAAAJ&hl=en), thank you for visiting this website 🙏

